import 'dart:convert';

import 'package:dynamically_generate_widget/json_templates/json_column.dart';
import 'package:dynamically_generate_widget/json_templates/json_container.dart';
import 'package:dynamically_generate_widget/json_templates/json_row.dart';
import 'package:dynamically_generate_widget/pages/dynamic_container.dart';
import 'package:dynamically_generate_widget/pages/dynamic_row.dart';
import 'package:dynamically_generate_widget/widgets/column_widget.dart';
import 'package:dynamically_generate_widget/widgets/container_widget.dart';
import 'package:dynamically_generate_widget/widgets/row_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Builder Page",
      home: DynamicContainer(),
      // home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List widgetText = [
    'Container',
    'Column',
    'Row',
    'Replace',
    'Delete',
  ];
  List<Widget> adds = [];

  ScrollController _scrollController = ScrollController();
  scrollToBottom() async {
    WidgetsBinding.instance!.addPostFrameCallback(
      (_) =>
          _scrollController.jumpTo(_scrollController.position.maxScrollExtent),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<dynamic> inputArray = adds;
    List<Map<String, dynamic>> jsonList = [];

    for (var widget in inputArray) {
      if (widget is DynamicRowsDemo) {
        jsonList.add(rowData);
      } else if (widget is Container) {
        jsonList.add(containerData);
      } else if (widget is Column) {
        jsonList.add(columnData);
      }
    }

    Map<String, dynamic> json = {"setting": jsonEncode(jsonList)};
    // print(json);
    Map<String, dynamic> widgetRowList(Map<String, dynamic> list) {
      return list;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Widget List",
          style: TextStyle(color: Colors.grey.shade50),
        ),
        backgroundColor: Colors.indigo.shade600,
        centerTitle: true,
        actions: [
          TextButton(
            onPressed: () {
              if (adds.isNotEmpty) {
                adds.clear();
                setState(() {});
              }
            },
            child: Text(
              'Clear',
              style: TextStyle(color: Colors.grey.shade50),
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            flex: 85,
            child: Container(
              child: RawScrollbar(
                thickness: 4,
                thumbColor: Colors.indigo,
                child: SingleChildScrollView(
                  controller: _scrollController,
                  child: Column(
                    children: adds,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 15,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey.shade300),
              ),
              width: double.infinity,
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                itemCount: widgetText.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: Chip(
                      backgroundColor: Colors.grey.shade100,
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 5),
                      label: Text(
                        widgetText[index],
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.indigo.shade600),
                      ),
                      elevation: 1,
                      side: BorderSide(color: Colors.indigo.shade600),
                    ),
                    onTap: () {
                      switch (index) {
                        case 0:
                          adds.add(containerWidget);
                          scrollToBottom();
                          setState(() {});
                          break;
                        // case 1:
                        //   adds.add(textWidget);
                        //   scrollToBottom();
                        //   setState(() {});
                        //   break;
                        case 1:
                          adds.add(columnWidget);
                          scrollToBottom();
                          setState(() {});
                          break;
                        case 2:
                          scrollToBottom();
                          setState(() {
                            adds.add(DynamicRowsDemo(
                              widgetList: widgetRowList,
                            ));
                          });
                          break;
                        case 3:
                          if (adds.isNotEmpty) {
                            adds[0] = rowWidget;
                            scrollToBottom();
                            setState(() {});
                          }
                          break;
                        case 4:
                          if (adds.isNotEmpty) {
                            adds.removeAt(0);
                            scrollToBottom();
                            setState(() {});
                          }
                          break;
                        default:
                      }
                    },
                  );
                },
                separatorBuilder: (context, index) {
                  return const SizedBox(
                    width: 10,
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
