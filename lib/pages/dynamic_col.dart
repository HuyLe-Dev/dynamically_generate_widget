// import 'package:flutter/material.dart';

// class DynamicColumn extends StatelessWidget {
//   final VoidCallback onRemove;

//   DynamicColumn({required this.onRemove});

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: 300,
//       height: 100,
//       color: Colors.blue,
//       margin: EdgeInsets.all(8.0),
//       child: Center(
//         child: Column(
//           children: [
//             Text('Column ${DateTime.now().millisecond}'),
//             ElevatedButton(
//               onPressed: onRemove,
//               child: Text('Remove Column'),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
import 'package:flutter/material.dart';

class DynamicColumn extends StatelessWidget {
  final VoidCallback onRemove;

  DynamicColumn({required Key key, required this.onRemove}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: 100,
      color: Colors.blue,
      margin: EdgeInsets.all(8.0),
      child: Center(
        child: Column(
          children: [
            Text('Column ${DateTime.now().millisecond}'),
            ElevatedButton(
              onPressed: onRemove, // Call the removeColumn callback
              child: Text('Remove Column'),
            ),
          ],
        ),
      ),
    );
  }
}
