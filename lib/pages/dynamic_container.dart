// import 'package:dynamically_generate_widget/pages/dynamic_row_widget.dart';
// import 'package:flutter/material.dart';

// class DynamicContainer extends StatefulWidget {
//   const DynamicContainer({super.key});

//   @override
//   _DynamicContainerState createState() => _DynamicContainerState();
// }

// class _DynamicContainerState extends State<DynamicContainer> {
//   List<DynamicRow> rows = [];

//   void addRow() {
//     setState(() {
//       rows.add(DynamicRow(
//         onRemove: () {
//           removeRow(rows.length - 1);
//         },
//       ));
//     });
//   }

//   void removeRow(int index) {
//     setState(() {
//       rows.removeAt(index);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         ElevatedButton(
//           onPressed: addRow,
//           child: Text('Add Row'),
//         ),
//         SizedBox(height: 16.0),
//         Container(
//           color: Colors.grey.shade200,
//           padding: EdgeInsets.all(16.0),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: rows,
//           ),
//         ),
//       ],
//     );
//   }
// }

import 'package:dynamically_generate_widget/pages/dynamic_row_widget.dart';
import 'package:flutter/material.dart';

class DynamicContainer extends StatefulWidget {
  const DynamicContainer({super.key});

  @override
  _DynamicContainerState createState() => _DynamicContainerState();
}

class _DynamicContainerState extends State<DynamicContainer> {
  List<DynamicRow> rows = [];

  void addRow() {
    final uniqueKey = UniqueKey();
    setState(() {
      rows.add(DynamicRow(
        key: uniqueKey, // Assign a UniqueKey to each row
        onRemove: () {
          removeRow(uniqueKey); // Pass the UniqueKey of the row to remove
        },
      ));
    });
  }

  void removeRow(Key key) {
    setState(() {
      rows.removeWhere((row) => row.key == key);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ElevatedButton(
          onPressed: addRow,
          child: Text('Add Row'),
        ),
        SizedBox(height: 16.0),
        Container(
          decoration: myBoxDecoration(),
          // color: Colors.grey.shade200,
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: rows,
          ),
        ),
      ],
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(
        color: Colors.red, //                   <--- border color
        width: 5.0,
      ),
    );
  }
}
