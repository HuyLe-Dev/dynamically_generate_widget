// import 'package:dynamically_generate_widget/pages/dynamic_col.dart';
// import 'package:flutter/material.dart';

// class DynamicRow extends StatefulWidget {
//   final VoidCallback onRemove;

//   DynamicRow({required this.onRemove});

//   @override
//   _DynamicRowState createState() => _DynamicRowState();
// }

// class _DynamicRowState extends State<DynamicRow> {
//   List<DynamicColumn> columns = [];

//   void addColumn() {
//     setState(() {
//       columns.add(DynamicColumn(
//         onRemove: () {
//           removeColumn(columns.length - 1);
//         },
//       ));
//     });
//   }

//   void removeColumn(int index) {
//     setState(() {
//       columns.removeAt(index);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SingleChildScrollView(
//       // Wrap with SingleChildScrollView
//       scrollDirection: Axis.horizontal, // Adjust the direction if needed
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           ElevatedButton(
//             onPressed: addColumn,
//             child: Text('Add Column'),
//           ),
//           SizedBox(height: 16.0),
//           Row(
//             children: [
//               ...columns,
//               ElevatedButton(
//                 onPressed: widget.onRemove,
//                 child: Text('Remove Row'),
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
// }

import 'package:dynamically_generate_widget/pages/dynamic_col.dart';
import 'package:flutter/material.dart';

class DynamicRow extends StatefulWidget {
  final VoidCallback onRemove;

  DynamicRow({required Key key, required this.onRemove}) : super(key: key);

  @override
  _DynamicRowState createState() => _DynamicRowState();
}

class _DynamicRowState extends State<DynamicRow> {
  List<DynamicColumn> columns = [];

  void addColumn() {
    final uniqueKey = UniqueKey();
    setState(() {
      columns.add(DynamicColumn(
        key: uniqueKey, // Assign a UniqueKey to each column
        onRemove: () {
          removeColumn(uniqueKey); // Pass the UniqueKey of the column to remove
        },
      ));
    });
  }

  void removeColumn(Key key) {
    setState(() {
      columns.removeWhere((column) => column.key == key);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: myBoxDecoration(),
      padding: EdgeInsets.all(16.0),
      margin: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ElevatedButton(
            onPressed: addColumn,
            child: Text('Add Column'),
          ),
          SizedBox(height: 16.0),
          Row(
            children: [
              ...columns,
              ElevatedButton(
                onPressed: widget.onRemove, // Call the removeRow callback
                child: Text('Remove Row'),
              ),
            ],
          ),
          SizedBox(height: 16.0),
        ],
      ),
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(
        color: Colors.black45, //                   <--- border color
        width: 5.0,
      ),
    );
  }
}
