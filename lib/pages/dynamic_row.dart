import 'package:flutter/material.dart';
import 'dart:convert';

class DynamicRowsDemo extends StatefulWidget {
  final Function(Map<String, dynamic>) widgetList;

  const DynamicRowsDemo({Key? key, required this.widgetList}) : super(key: key);

  @override
  _DynamicRowsDemoState createState() => _DynamicRowsDemoState();
}

class _DynamicRowsDemoState extends State<DynamicRowsDemo> {
  // List<Widget> rows = [];
  List<Widget> rows = [];

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> jsonDataList = rows.map((Widget widget) {
      if (widget is Row) {
        Row rowWidget = widget;
        return {
          "widgetName": "Row",
          "widgetSetting": {
            "direction": "horizontal",
            "mainAxisAlignment":
                rowMainAxisAlignmentToString(rowWidget.mainAxisAlignment),
            "crossAxisAlignment":
                rowCrossAxisAlignmentToString(rowWidget.crossAxisAlignment),
          },
        };
      }
      return <String,
          dynamic>{}; // Specify the type explicitly for the empty map
    }).toList();

    Map<String, dynamic> jsonData = {"children": jsonDataList};
    // Convert the jsonData to a JSON string for printing

    Map<String, dynamic> jsonString = {"setting": jsonEncode(jsonData)};
    print(jsonString);

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ElevatedButton(
          onPressed: () {
            setState(
              () {
                rows.add(
                  buildRow(jsonString), // Create and add a new row
                );
                widget.widgetList(jsonString);
              },
            );
          },
          child: Text('Add Row'),
        ),
        SizedBox(height: 16.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: rows,
        ),
      ],
    );
  }

  Widget buildRow(Map<String, dynamic> jsonString) {
    final UniqueKey rowKey = UniqueKey(); // Generate a unique key for the row
    return Row(
      key: rowKey,
      children: [
        Text('Row ${rows.length + 1}'),
        ElevatedButton(
          onPressed: () {
            setState(() {
              // Remove the row with the specified key
              rows.removeWhere((element) => element.key == rowKey);
              widget.widgetList(jsonString);
            });
          },
          child: Text('Delete'),
        ),
      ], // Assign the unique key to the row
    );
  }

  String rowMainAxisAlignmentToString(MainAxisAlignment alignment) {
    switch (alignment) {
      case MainAxisAlignment.start:
        return "start";
      case MainAxisAlignment.end:
        return "end";
      case MainAxisAlignment.center:
        return "center";
      // Add more cases for other alignment values as needed
      default:
        return "start"; // Default to "start" if not recognized
    }
  }

  String rowCrossAxisAlignmentToString(CrossAxisAlignment alignment) {
    switch (alignment) {
      case CrossAxisAlignment.start:
        return "start";
      case CrossAxisAlignment.end:
        return "end";
      case CrossAxisAlignment.center:
        return "center";
      // Add more cases for other alignment values as needed
      default:
        return "center"; // Default to "center" if not recognized
    }
  }
}
