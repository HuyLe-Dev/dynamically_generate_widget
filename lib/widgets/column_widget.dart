import 'package:flutter/material.dart';

Widget columnWidget = Column(
  children: [
    Text(
      "I am Column 1",
      style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: Colors.indigo.shade600),
    ),
    const SizedBox(
      height: 10,
    ),
    Text(
      "I am Column 2",
      style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: Colors.indigo.shade600),
    ),
    const SizedBox(
      height: 10,
    ),
  ],
);
