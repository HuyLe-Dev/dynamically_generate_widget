import 'package:flutter/material.dart';

class MyInheritedWidget extends InheritedWidget {
  final List<Widget> widgetList;

  MyInheritedWidget({
    required this.widgetList,
    required Widget child,
  }) : super(child: child);

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return true;
  }

  static MyInheritedWidget? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<MyInheritedWidget>();
  }
}
